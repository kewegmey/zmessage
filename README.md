# Zmessage
A ZMQ based messaging application.

Right now it's just an idea :) Some nice things to have would be: 

* End to end encryption
* Full transport encryption and authentication via curve zmq
* Android and iOS applications
* Implement both server and p2p patterns
* Decent python based graphical application

# Thoughts on Implementation

## Levels of keys
* Clients will use curveZMQ to secure their connection to the server.  This is similar to TLS when using XMPP.  It is transport encryption.  Additionally, this allows the users to verify the server and the server to verify the users.  Furthermore, there will be end to end encryption based on some PKI (perhaps RSA) that can be used to secure comms as well as verify users to eachother.  The server will also maintain a keypair in this second level so that special control messages can be securely sent to the server.  

## Key Registration
Just after a client starts up it will send a special message encrypted with server pubkey over REQ/REP to server.  The message will contain the username of the client.  The server will register this username with the pubkey of the user.  This way other clients can discover the pubkey from a username.  Clients should be asked to verify the fingerprint of the public key.    

## Send messages
* Client connects as REQ to server REP.  When a message is to be sent it sends the REQ and the server replies that it got the message.
* Client will use RSA keys to prove identity - all messages will be encrypted to dest and signed buy sender.     

## Recv Messages
* Client connects as SUB to server PUB.  This is a public bus that all clients can see although they should only subscribe to messages pertaining to them.  When server has message to send to client it publishes a notice that the client should send a special message over its REQ/REP which will solicit a response contianing the message.  The server will verify that the client is the actual recipient.  Client recv messages will decrypt the message with its private key and send another special REQ notifying the server that the message was recvd.  

## Message types
* Message send:  Client wants to send a message to another user.  Will determine dest public key from key registry if not already stored on client.  Message will be encrypted with dest pubkey and signed by sender and send to server.  Server acks message recv on reply. 
* Message recv solicit: Server has message for a specific pubkey.  Server broadcasts announcement to topic (PUB) indicating it has a message for that key available.  Message signed with server key, not encrypted.
* Message recv: Client hears on SUB that server has message for it and verifies the message sig then sends a message over REQ to retreieve that message.  This message signed by client (consider encryption?).  Server verifies that the dest pubkey from the message matches the client that responded to the recv solicit.  Server then forwards the message content with the reply.  Client uses its private key to decrypt the message.

## Conferences
* e2e encryption becomes much more complicated.  Would follow the same pattern as user to user messaging but would involve broadcast encryption.

## Forward secrecy
* There is none with this model.  Could switch to using a session key and AES for message encryption and using the PKI for sharing the key.  In effort to not "roll my own crypto" could alternatively implement ECDH.  

# Other sketchy ideas
Instead of a pubkey to username mapping what if it was all client based.  So the sender knows the username of the dest and sends a special message asking the server "hey I need so and so's key (so far the same as with a key repo) but then the server turns aroud and says "hey kory,pubkey:123 needs the pubkey for the user alice."  Alice hears this on the SUB and sends a message to kory's public key so that kory knows her pubkey.  One issue is users could try to use the same username and could trick kory into sending messages to the wrong person.  for now lets stick with the server being the sudo source of truth.  Users should still verify keys manually!