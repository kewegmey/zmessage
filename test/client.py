import zmq
import json
import sys
import time
import zmq.auth
import os
import rsa
import multiprocessing
import binascii

# CLI based client
# will eventually drive GUI
def gen_key():
    # forces 4096 length and uses max CPUs
    print "Generating RSA keypair"
    (pubkey, privkey) = rsa.newkeys(4096, poolsize=(multiprocessing.cpu_count()))
    return pubkey,privkey

def gen_key_file():
    pubkey, privkey = gen_key()
    pub = open('pub.key','w')
    priv = open('priv.key','w')
    pub.write(pubkey.save_pkcs1('PEM'))
    priv.write(privkey.save_pkcs1('PEM'))
    pub.close()
    priv.close()

def sign_msg(message, privkey):
    signature = binascii.hexlify(rsa.sign(message, privkey, 'SHA-512'))
    return signature

def enc_msg(message, destpubkey):
    ct = binascii.hexlify(rsa.encrypt(message, destpubkey))
    return ct

def usr_reg(username, pubkey, privkey):
    # pubkey and privkey are non serializable so we need to convert them
    pubkeystr = pubkey.save_pkcs1('PEM')
    reg = {}
    reg['type'] = 'usr_reg'
    reg['message'] = {}
    reg['message']['username'] = username
    reg['message']['pubkey'] = pubkeystr
    # Serialize to json for signing
    ser_msg = json.dumps(reg['message'])
    #print "User meessage to send: " + ser_msg
    # The signature is binary so convert to hex to send
    reg['signature'] = sign_msg(ser_msg, privkey)
    #print reg
    return reg

def key_lookup(socket, username):
    # Check local store of keys for key in question
    if username in pubkeys:
        print "Using local copy of user pubkey"
        return pubkeys[username]
    else:
        # Queries for user pubkey.  Returns pubkey obj if possible or returns dict notifying there was a problem
        print "Reaching out to server to get user pubkey"
        lookup = {}
        lookup['type'] = 'key_lookup'
        lookup['message'] = username
        lookup['signature'] = ''
        # Get pubkey of dest
        socket.send_json(lookup)
        reply = socket.recv_json()
        try:
            destpub = reply[username]
            destpubo = rsa.PublicKey(5,3)
            destpub = destpubo.load_pkcs1(destpub, 'PEM')
            # Save key locally
            pubkeys[username] = destpub
            return destpub
        except:
            return reply

def regular_msg(destpubkey, message, privkey, socket):
    # Builds encrypted message for remote user.
    regular = {}
    regular['type'] = 'regular_msg'
    # Encrypt message to dest
    message = message.encode('utf8')
    regular['message'] = enc_msg(message, destpubkey)
    # Sign message
    regular['signature'] = sign_msg(regular['message'], privkey)
    socket.send_json(regular)
    reply = socket.recv_json()
    return reply

def main():
    port = "5555"
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    # Sec part
    client_secret_file = os.path.abspath("keys/client.key_secret")
    client_public, client_secret = zmq.auth.load_certificate(client_secret_file)
    socket.curve_secretkey = client_secret
    socket.curve_publickey = client_public

    server_public_file = os.path.abspath("keys/server.key")
    server_public, _ = zmq.auth.load_certificate(server_public_file)
    # The client must know the server's public key to make a CURVE connection.
    socket.curve_serverkey = server_public
    socket.connect("tcp://localhost:%s" % port)

    # Temporarily just gen keys at startup and hold in memory
    # pubkey, privkey = gen_key()

    # Load Keys
    try:
        pub = (open("pub.key")).read()
        priv = (open("priv.key")).read()
        pubkeyo = rsa.PublicKey(5,3)
        pubkey = pubkeyo.load_pkcs1(pub, format='PEM')
        privkeyo = rsa.PrivateKey(3247, 65537, 833, 191, 17)
        privkey = privkeyo.load_pkcs1(priv, format='PEM')
    except:
        gen_key_file()
        pub = (open("pub.key")).read()
        priv = (open("priv.key")).read()
        pubkeyo = rsa.PublicKey(5,3)
        pubkey = pubkeyo.load_pkcs1(pub, format='PEM')
        privkeyo = rsa.PrivateKey(3247, 65537, 833, 191, 17)
        privkey = privkeyo.load_pkcs1(priv, format='PEM')



    # Static username for now too
    username = 'Kory'

    # Init - check if we are still registered with server
    registered = False
    while not registered:
        print "Checking if we are still registered..."
        reply = key_lookup(socket, username)
        print reply
        if reply == {'status': 'User not found'} or reply != pubkey:
        # Check that the key stored in the server is correct
            # Register username and key with server
            print "Registering with server"
            jsono = usr_reg(username, pubkey, privkey)
            # Simulate someone changing the message in flight
            #jsono['message']['username'] = 'cat'
            socket.send_json(jsono)
            reply = socket.recv_json()
            if reply == {'status': 'Successfully registered.'}:
                registered = True
            time.sleep(1)
        else:
            print "Still registered."
            registered = True


    while True:
        to = 'Kory'
        destpub = key_lookup(socket, to)
        reply = regular_msg(destpubkey=destpub, message='Hello friend', privkey=privkey, socket=socket)

        print "Reply is: " + str(reply)
        #time.sleep(2)

# Unlike server side this is a store of key objects saved this way for speed.  When we go to write these out to a file then we will save as PEM
pubkeys = {}

if __name__ == "__main__":
    main()